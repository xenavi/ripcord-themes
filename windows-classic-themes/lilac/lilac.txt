{
    "alternate_base": "#9692bc",
    "base": "#bfbaef",
    "button": "#9692bc",
    "chat_timestamp": "#000000",
    "disabled_button": "#7a7799",
    "disabled_icon": "#7a7799",
    "disabled_text": "#514f66",
    "highlight": "#5a4eb1",
    "highlighted_text": "#ffffff",
    "icon": "#000000",
    "text": "#000000",
    "unread_badge": "#5a4eb1",
    "unread_badge_text": "#ffffff",
    "window": "#aea8d9"
}