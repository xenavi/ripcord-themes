{
    "alternate_base": "#d0bf98",
    "base": "#f9e4b3",
    "button": "#d0bf98",
    "chat_timestamp": "#000000",
    "disabled_button": "#c5b591",
    "disabled_icon": "#c5b591",
    "disabled_text": "#8c7f66",
    "highlight": "#800080",
    "highlighted_text": "#ffffff",
    "icon": "#000000",
    "text": "#000000",
    "unread_badge": "#800080",
    "unread_badge_text": "#ffffff",
    "window": "#ecd59d"
}