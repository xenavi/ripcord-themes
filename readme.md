This is a collection of themes I've made for Ripcord.

To use one of the themes here:
1. Copy the contents of the theme's .txt file.
2. In Ripcord, go to View > Preferences...
3. Go to the Style tab and set Ripcord's theme to "Custom".
4. Click the Customize button to the right of the theme menu.
5. Click "Paste from JSON".

