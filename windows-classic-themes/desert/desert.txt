{
    "alternate_base": "#a39d90",
    "base": "#f7edda",
    "button": "#a39d90",
    "chat_timestamp": "#000000",
    "disabled_button": "#908b80",
    "disabled_icon": "#908b80",
    "disabled_text": "#68645c",
    "highlight": "#008078",
    "highlighted_text": "#ffffff",
    "icon": "#000000",
    "text": "#000000",
    "unread_badge": "#008078",
    "unread_badge_text": "#ffffff",
    "window": "#d5ccbb"
}