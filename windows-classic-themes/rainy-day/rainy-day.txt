{
    "alternate_base": "#778ca1",
    "base": "#92abc5",
    "button": "#778ca1",
    "chat_timestamp": "#000000",
    "disabled_button": "#6e8194",
    "disabled_icon": "#6e8194",
    "disabled_text": "#566574",
    "highlight": "#4f657d",
    "highlighted_text": "#ffffff",
    "icon": "#000000",
    "text": "#000000",
    "unread_badge": "#4f657d",
    "unread_badge_text": "#ffffff",
    "window": "#8399b1"
}